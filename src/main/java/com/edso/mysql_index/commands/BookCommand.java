package com.edso.mysql_index.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BookCommand {
    private Long id;
    private String name;
    private String typeCode;
}
