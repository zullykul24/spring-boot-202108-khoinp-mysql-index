package com.edso.mysql_index.repositories;

import com.edso.mysql_index.domains.BookIndex;
import com.edso.mysql_index.domains.BookSaveAll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Repository
public interface BookSaveAllRepository extends JpaRepository<BookSaveAll, Long> {
    @Query( value = "select * from book_save_all where type_code like 'a%'", nativeQuery = true)
    Set<Optional<BookIndex>> findStartByA();

    @Transactional
    @Modifying
    @Query(value = "create index idx_type_code on book_save_all(type_code(1))", nativeQuery = true)
    void addIndex();
}
