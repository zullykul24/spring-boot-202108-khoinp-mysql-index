package com.edso.mysql_index.repositories;

import com.edso.mysql_index.domains.BookIndex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Repository
public interface BookIndexRepository extends JpaRepository<BookIndex, Long> {
    @Query( value = "select * from book_index where type_code like 'a%'", nativeQuery = true)
    Set<Optional<BookIndex>> findStartByA();

    @Transactional
    @Modifying
    @Query(value = "create index idx_type_code on book_index(type_code(1))", nativeQuery = true)
    void addIndex();
}
