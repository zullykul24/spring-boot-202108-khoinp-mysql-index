package com.edso.mysql_index.repositories;

import com.edso.mysql_index.domains.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.Optional;
import java.util.Set;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    @Override
    Optional<Book> findById(Long aLong);

    @Query( value = "select * from book where type_code like 'a%'", nativeQuery = true)
    Set<Optional<Book>> findStartByA();




}
