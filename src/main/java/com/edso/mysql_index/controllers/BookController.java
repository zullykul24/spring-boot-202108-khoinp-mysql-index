package com.edso.mysql_index.controllers;

import com.edso.mysql_index.domains.Book;
import com.edso.mysql_index.repositories.BookRepository;
import com.edso.mysql_index.services.BookService;
import com.edso.mysql_index.services.BookServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;


@Controller
public class BookController {
    private final BookService bookService;
    private final BookRepository bookRepository;


    public BookController(BookService bookService, BookRepository bookRepository) {
        this.bookService = bookService;
        this.bookRepository = bookRepository;
    }

    @GetMapping
    @RequestMapping({"","/","/book"})
    public String getIndexPage(){
        Long before = System.currentTimeMillis();
        //bookService.performAddNumbersOfRecords(2000000L);
        Set<Optional<Book>> book = bookRepository.findStartByA();
        Long after = System.currentTimeMillis();
        System.out.println(after-before + " milliseconds");
        return "index";
    }
}
