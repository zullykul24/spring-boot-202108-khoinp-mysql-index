package com.edso.mysql_index.controllers;

import com.edso.mysql_index.domains.BookIndex;
import com.edso.mysql_index.repositories.BookIndexRepository;
import com.edso.mysql_index.repositories.BookSaveAllRepository;
import com.edso.mysql_index.services.BookIndexService;
import com.edso.mysql_index.services.BookSaveAllService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;

@Controller
public class BookSaveAllController {
    private final BookSaveAllService bookSaveAllService;
    private final BookSaveAllRepository bookSaveAllRepository;

    public BookSaveAllController(BookSaveAllService bookSaveAllService,
                                 BookSaveAllRepository bookSaveAllRepository) {
        this.bookSaveAllService = bookSaveAllService;
        this.bookSaveAllRepository = bookSaveAllRepository;
    }

    @GetMapping
    @RequestMapping({"/book_save_all"})
    public String getIndexPage(){
//        bookIndexRepository.addIndex();
        Long before = System.currentTimeMillis();
        bookSaveAllService.performAddNumbersOfRecords(2000000L);
//        Set<Optional<BookIndex>> bookIndexes = bookIndexRepository.findStartByA();
        Long after = System.currentTimeMillis();
        System.out.println(after-before + " milliseconds");
        return "index";
    }
}