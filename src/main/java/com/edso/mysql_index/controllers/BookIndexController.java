package com.edso.mysql_index.controllers;

import com.edso.mysql_index.domains.Book;
import com.edso.mysql_index.domains.BookIndex;
import com.edso.mysql_index.repositories.BookIndexRepository;
import com.edso.mysql_index.services.BookIndexService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;

@Controller
public class BookIndexController {
    private final BookIndexService bookIndexService;
    private final BookIndexRepository bookIndexRepository;

    public BookIndexController(BookIndexService bookIndexService, BookIndexRepository bookIndexRepository) {
        this.bookIndexService = bookIndexService;
        this.bookIndexRepository = bookIndexRepository;
    }

    @GetMapping
    @RequestMapping({"/book_index"})
    public String getIndexPage(){
//        bookIndexRepository.addIndex();
        Long before = System.currentTimeMillis();
//        bookIndexService.performAddNumbersOfRecords(2000000L);
        Set<Optional<BookIndex>> bookIndexes = bookIndexRepository.findStartByA();
        Long after = System.currentTimeMillis();
        System.out.println(after-before + " milliseconds");
        return "index";
    }
}
