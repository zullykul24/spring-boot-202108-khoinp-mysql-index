package com.edso.mysql_index.services;

import com.edso.mysql_index.domains.BookIndex;
import com.edso.mysql_index.domains.BookSaveAll;
import com.edso.mysql_index.repositories.BookSaveAllRepository;
import com.edso.mysql_index.utils.GenerateTypeCode;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class BookSaveAllServiceImpl implements BookSaveAllService {
    private final BookSaveAllRepository bookSaveAllRepository;

    public BookSaveAllServiceImpl(BookSaveAllRepository bookSaveAllRepository) {
        this.bookSaveAllRepository = bookSaveAllRepository;
    }

//    @Override
//    public void addRecord(String name, String typeCode) {
//        bookSaveAllRepository.save(new BookSaveAll(name, typeCode));
//    }

    @Override
    @Transactional
    public void performAddNumbersOfRecords(Long number) {
        List<BookSaveAll> list = new ArrayList<>();
        for(int i = 0; i< number; i++){
            list.add(new BookSaveAll("book_" + i,
                    RandomStringUtils.randomNumeric(1)+ RandomStringUtils.randomAlphabetic(2)));
        }

        bookSaveAllRepository.saveAll(list);
    }
}
