package com.edso.mysql_index.services;

public interface BookIndexService {
    void addRecord(String name, String typeCode);

    void performAddNumbersOfRecords(Long number);
}
