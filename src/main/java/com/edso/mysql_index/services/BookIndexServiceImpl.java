package com.edso.mysql_index.services;

import com.edso.mysql_index.domains.Book;
import com.edso.mysql_index.domains.BookIndex;
import com.edso.mysql_index.repositories.BookIndexRepository;
import com.edso.mysql_index.utils.GenerateTypeCode;
import com.edso.mysql_index.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookIndexServiceImpl implements BookIndexService {
    private final BookIndexRepository bookIndexRepository;

    public BookIndexServiceImpl(BookIndexRepository bookIndexRepository) {
        this.bookIndexRepository = bookIndexRepository;
    }

    @Override
    public void addRecord(String name, String typeCode) {

        bookIndexRepository.save(new BookIndex(name, typeCode));
    }

    @Override
    @Transactional
    public void performAddNumbersOfRecords(Long number) {

        for(int i = 0; i< number; i++){
            addRecord(GenerateTypeCode.randomBookName(7,20), GenerateTypeCode.randomTypeCode(4));
        }
    }


}
