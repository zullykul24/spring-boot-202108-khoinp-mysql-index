package com.edso.mysql_index.services;

import com.edso.mysql_index.domains.Book;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.OptionalInt;

public interface BookService {
    Optional<Book> findById(Long id);

    void addRecord(String name, String typeCode);

    void performAddNumbersOfRecords(Long number);

}
