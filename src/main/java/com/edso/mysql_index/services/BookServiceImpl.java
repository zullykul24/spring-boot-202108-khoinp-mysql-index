package com.edso.mysql_index.services;


import com.edso.mysql_index.domains.Book;
import com.edso.mysql_index.repositories.BookRepository;
import com.edso.mysql_index.utils.GenerateTypeCode;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Optional<Book> findById(Long id){
        Optional<Book> bookOptional = bookRepository.findById(id);
        if(!bookOptional.isPresent()){
            throw new RuntimeException("Book id not found");
        }

        return bookOptional;
    }

    @Override
    public void addRecord(String name, String typeCode) {
        bookRepository.save(new Book(name, typeCode));
    }

    @Override
    @Transactional
    public void performAddNumbersOfRecords(Long number) {
        for(int i = 0; i< number; i++){
            addRecord(GenerateTypeCode.randomBookName(7,20), GenerateTypeCode.randomTypeCode(4));
        }
    }


}
