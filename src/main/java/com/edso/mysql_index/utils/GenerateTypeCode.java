package com.edso.mysql_index.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class GenerateTypeCode {
    public static String randomTypeCode(int length){
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String randomBookName(int minimum, int maximum){
        Random rn = new Random();
        int range = maximum - minimum + 1;
        int randomNum =  rn.nextInt(range) + minimum;

        return RandomStringUtils.randomAlphabetic(randomNum);
    }
}
