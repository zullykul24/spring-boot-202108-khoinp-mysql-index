package com.edso.mysql_index.domains;

import lombok.Data;


import javax.persistence.*;


//@Table(name = "book_index", indexes = @Index(name = "idx", columnList = "type_code"))
@Entity
@Data
@Table
public class BookIndex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type_code")
    private String typeCode;

    public BookIndex(){}

    public BookIndex(String name, String typeCode){
        this.name = name;
        this.typeCode = typeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookIndex book = (BookIndex) o;

        return id != null ? id.equals(book.id) : book.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
