package com.edso.mysql_index.domains;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table
public class BookSaveAll {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type_code")
    private String typeCode;

    public BookSaveAll(){}

    public BookSaveAll(String name, String typeCode){
        this.name = name;
        this.typeCode = typeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookSaveAll book = (BookSaveAll) o;

        return id != null ? id.equals(book.id) : book.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
