package com.edso.mysql_index.converters;

import com.edso.mysql_index.commands.BookCommand;
import com.edso.mysql_index.domains.Book;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BookCommandToBook implements Converter<BookCommand, Book> {
    @Override
    public Book convert(BookCommand source) {
        if(source == null) return null;
        final Book book = new Book();
        book.setId(source.getId());
        book.setName(source.getName());
        book.setTypeCode(source.getTypeCode());

        return book;
    }
}
