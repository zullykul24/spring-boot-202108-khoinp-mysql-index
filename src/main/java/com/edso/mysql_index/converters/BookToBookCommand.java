package com.edso.mysql_index.converters;

import com.edso.mysql_index.commands.BookCommand;
import com.edso.mysql_index.domains.Book;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class BookToBookCommand implements Converter<Book, BookCommand> {
    @Override
    public BookCommand convert(Book source) {
        if(source == null) return null;
        final BookCommand bookCommand = new BookCommand();
        bookCommand.setId(source.getId());
        bookCommand.setName(source.getName());
        bookCommand.setTypeCode(source.getTypeCode());

        return bookCommand;
    }
}
