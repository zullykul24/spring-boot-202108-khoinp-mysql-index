package com.edso.mysql_index;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlIndexApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqlIndexApplication.class, args);
    }

}
